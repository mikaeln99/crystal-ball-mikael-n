package android.nortikyanm.crystalball;

import java.util.Random;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will never come true.",
                "You worry too much about it, what will be, will be...",
                "You know you got some life problems when your asking an app a question",
                "Not with that attitude",
                "Aren't you supposed to be coding or something right now?",
                "Now how the hell am I supposed to know what.."

        };
    }

    public static Predictions get() {
        if (predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public  String getPrediction(){
        int rnd = new Random().nextInt(answers.length);
        return answers[rnd];
    }

}